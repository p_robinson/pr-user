var bcrypt_1 = require('bcrypt');
function hashPassword(password) {
    return new Promise(function (resolve, reject) {
        bcrypt_1.genSalt(10, function (err, salt) {
            if (err) {
                return reject(err);
            }
            bcrypt_1.hash(password, salt, function (err, hash) {
                if (err) {
                    return reject(err);
                }
                resolve(hash);
            });
        });
    });
}
function beforeCreate(user, options, next) {
    hashPassword(user.password)
        .then(function (hash) {
        user.password = hash;
        next();
    })
        .catch(next);
}
exports.beforeCreate = beforeCreate;
function beforeUpdate(user, options, next) {
    if (user.password) {
        hashPassword(user.password)
            .then(function (hash) {
            user.password = hash;
            next();
        })
            .catch(next);
    }
    else {
        next();
    }
}
exports.beforeUpdate = beforeUpdate;
