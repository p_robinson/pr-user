var sequelize = require('sequelize');
exports.attributes = {
    email: {
        type: sequelize.STRING,
        unique: true,
        allowNull: false,
        validate: {
            isEmail: { msg: 'Not a Valid Email' }
        }
    },
    password: {
        type: sequelize.STRING,
        allowNull: false
    },
    resetToken: {
        type: sequelize.STRING,
    },
    resetExpire: {
        type: sequelize.DATE
    },
    failAttempts: {
        type: sequelize.INTEGER,
        defaultValue: 0
    },
    status: {
        type: sequelize.STRING,
        defaultValue: 'active'
    }
};
