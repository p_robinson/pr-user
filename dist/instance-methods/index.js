var bcrypt_1 = require('bcrypt');
var crypto = require('crypto');
function checkPassword(candidatePassword) {
    var _this = this;
    return new Promise(function (resolve, reject) {
        bcrypt_1.compare(candidatePassword, _this.password, function (err, isMatch) {
            if (err)
                reject(err);
            else
                resolve(isMatch);
        });
    });
}
exports.checkPassword = checkPassword;
function updatePassword(oldPassword, newPassword) {
    var _this = this;
    return new Promise(function (resolve, reject) {
        _this.checkPassword(oldPassword).then(function (isMatch) {
            if (!isMatch) {
                reject(new Error('Passwords Do Not Match'));
            }
            else {
                resolve(_this.resetPassword(newPassword));
            }
        });
    });
}
exports.updatePassword = updatePassword;
function resetPassword(password) {
    var _this = this;
    return new Promise(function (resolve, reject) {
        bcrypt_1.genSalt(function (err, salt) {
            if (err)
                reject(err);
            else {
                bcrypt_1.hash(password, salt, function (err, hash) {
                    if (err)
                        reject(err);
                    else {
                        resolve(_this.update({ password: hash }));
                    }
                });
            }
        });
    });
}
exports.resetPassword = resetPassword;
function createReset() {
    var _this = this;
    return new Promise(function (resolve, reject) {
        var now = new Date();
        now.setHours(now.getHours() + 1);
        crypto.randomBytes(48, function (err, buf) {
            if (err)
                return reject(err);
            var token = buf.toString('hex');
            resolve(_this.update({ resetToken: token, resetExpire: now, status: 'reset' }));
        });
    });
}
exports.createReset = createReset;
function loginFail() {
    return this.increment('failAttempts', { by: 1 })
        .then(function (user) {
        return user.reload();
    });
}
exports.loginFail = loginFail;
function loginSuccess() {
    return this.update({ failAttempts: 0 });
}
exports.loginSuccess = loginSuccess;
function activateAccount() {
    var activate = {
        failAttempts: 0,
        status: 'active',
        resetToken: null,
        resetExpire: null
    };
    return this.update(activate);
}
exports.activateAccount = activateAccount;
function deactivateAccount() {
    var deactivate = {
        status: 'inactive'
    };
    return this.update(deactivate);
}
exports.deactivateAccount = deactivateAccount;
