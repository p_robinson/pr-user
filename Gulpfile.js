var gulp = require('gulp')
var plugins = require('gulp-load-plugins')()
var del = require('del')
var inquirer = require('inquirer')

var merge = require('merge2');


gulp.task('clean:test', cb => {
  return del(['tmp'], cb);
});
gulp.task('clean:dist', cb => {
  return del(['dist', 'definitions'], cb);
});

gulp.task('default', () => {
  plugins.watch('src/**/index.ts', {
    ignoreInitial: true,
    events: ['add']
  }, (vinyl) => {
    var mod = vinyl.dirname.split('/').pop()
    gulp.src('src/index.ts')
    .pipe(plugins.insert.append(`\nimport ${mod} = require('./${mod}')\nexportModule['${mod}'] = ${mod}`))
    .pipe(gulp.dest('src'))
  })
})
gulp.task('compile:test', ['clean:test'], () => {
  return gulp.src(['src/**/**.ts', 'typings/**/*.ts'])
    .pipe(plugins.typescript({
      target: 'es5',
      module: 'commonjs',
      moduleResolution: 'node',
      outDir: 'test'
    }))
    .pipe(gulp.dest('tmp/test'))
})
gulp.task('compile:dist', ['clean:dist'], () => {
    var tsResult = gulp.src(['src/**/**.ts', 'typings/**/*.ts', '!src/**/**.spec.ts'])
        .pipe(plugins.typescript({
            target: 'es5',
            module: 'commonjs',
            moduleResolution: 'node',
            outDir: 'dist',
            declaration: false,
            noExternalResolve: true
        }));

    return merge([
        tsResult.dts.pipe(gulp.dest('definitions')),
        tsResult.js.pipe(gulp.dest('dist'))
    ]);
})

gulp.task('dist', ['compile:dist'], () => {
  return
})
gulp.task('test', ['compile:test'], () => {
  return gulp.src(['tmp/test/**/**.spec.js', 'tmp/test/index.spec.js'], {read:false})
  .pipe(plugins.mocha({
    require: ['chai'],
    reporter: 'spec'
  }))

})

gulp.task('bump', () => {
  return inquirer.prompt([
    {
      type: 'list',
      name: 'type',
      message: 'What type of version bump?',
      choices: [
        'Major',
        'Minor',
        'Patch',
        'Release',
        'Pre Release'
      ],
      filter: val => {
        return val.toLowerCase().replace(' ', '')
      }
    }],
    answer => {
      return gulp.src(['bower.json', 'package.json'])
      .pipe(plugins.bump(answer))
      .pipe(gulp.dest('./'))


      }
  )
})
