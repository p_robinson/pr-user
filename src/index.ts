import * as instanceMethods from './instance-methods';
import * as classMethods from './class-methods';
import * as attributes from './attributes';
import * as hooks from './hooks';
import * as defaultScope from './default-scope';
import * as getterMethods from './getter-methods';
export { instanceMethods, classMethods, attributes, hooks, getterMethods }

export var modelOptions = { instanceMethods, classMethods, hooks, defaultScope, getterMethods}

