import { instanceMethods, classMethods, attributes, hooks, getterMethods }from './';
import * as chai from 'chai';
import * as sequelize from 'sequelize';
interface UserAttributes extends attributes.IAttributes { };
interface UserInstance extends sequelize.Instance<UserInstance, UserAttributes>, instanceMethods.IInstanceMethods<UserInstance> { }
interface UserModel<TInstance, TAttributes> extends sequelize.Model<TInstance, TAttributes> {

};

var s = new sequelize('test', 'root', null, {
  host: '127.0.0.1',
  dialect: 'mysql',
  logging: false
});

var User = <UserModel<UserInstance, UserAttributes>>s.define('User', attributes.attributes, { instanceMethods, hooks });


describe('prUser', () => {
  before(done => {
    s.sync({ 'force': true })
    .then(() => {
      done();
    });
  })
  it('should pass', done => {

    done()
  })

  describe('InstanceMethods', function () {
    this.timeout(5000)
    var userInst: UserInstance;
    beforeEach(done => {
      User.create({email:'paul.robinson@kyndling.com', password:'password'})
      .then(user => {
        userInst = user;
        done()
      }).catch(done)
    })
    describe('resetPassword', () => {
      it('reset password to Password', done => {
        userInst.resetPassword('Password')
        .then(user => {
          chai.expect(user).to.exist
          done()
        }).catch(done)
      })
    })
    describe('checkPassword', () => {
      it('should return true', done => {
        userInst.checkPassword('password')
        .then(isMatch => {
          chai.expect(isMatch).to.be.true
          done()
        })
      })
      it('should return false', done => {
        userInst.checkPassword('Password')
        .then(isMatch => {
          chai.expect(isMatch).to.be.false
          done()
        })
      })
    })
    describe('updatePassword', () => {
      it('update with correct Password', done => {
        userInst.updatePassword('password', 'Password')
        .then(user => {
          user.checkPassword('Password')
          .then(isMatch => {
            chai.expect(isMatch).to.be.true
            done()
          }).catch(done)
        }).catch(done)
      })
    })
    describe('loginFail', () => {
      it('update failed login attempts', done => {
        userInst.loginFail()
        .then(user => {
          chai.expect(user.get('failAttempts')).to.equal(1)
          done()
        }).catch(done)
      })
    })
    describe('loginSuccess', () => {
      it('clear login attempts after loginFail', done => {
        userInst.loginFail()
        .then(user => {
          user.loginSuccess().then(user => {
            chai.expect(user.get('resetExpire')).to.be.null
            chai.expect(user.get('failAttempts')).to.equal(0)
            chai.expect(user.get('resetToken')).to.be.null
            done()
          }).catch(done)
      }).catch(done)
      })
    })
    describe('activateAccount', () => {
      before(done => {
      userInst.deactivateAccount().then(user => {
        userInst = user
        done()
        }).catch(done)
      })
      it('Activate Account from Deactive', done =>{
        userInst.activateAccount().then(user => {
          chai.expect(user.get('status')).to.equal('active')
          done()
        }).catch(done)
      })
    })
    describe('createReset', () => {
      it('should put user into state of reset', done => {
        userInst.createReset()
        .then(user => {
          chai.expect(user.get('resetToken')).not.to.be.null
          chai.expect(user.get('resetExpire')).not.to.be.null
          chai.expect(user.get('status')).to.equal('reset')
          done()
        })
      })
    })
    afterEach(done => {
      User.truncate()
      .then(() => {
        done()
      }).catch(done)
    })
  })
})
