import { genSalt, hash } from 'bcrypt';

export interface HooksInterface {

}
function hashPassword (password) {
  return new Promise ((resolve, reject) => {
    genSalt(10, (err, salt) => {
      if (err) {
        return reject(err);
      }
      hash(password, salt, (err, hash) => {
        if (err) {
          return reject(err);
        }
        resolve(hash);
      });
    });
  })
}
export function beforeCreate (user, options, next) {
  hashPassword(user.password)
    .then(hash => {
      user.password = hash
      next()
    })
    .catch(next)
}
export function beforeUpdate (user, options, next) {
  if (user.password) {
    hashPassword(user.password)
    .then(hash => {
      user.password = hash
      next()
    })
    .catch(next)
  }
  else {
    next()
  }
}
