import { compare, hash, genSalt } from 'bcrypt';
var crypto = require('crypto');

export interface IInstanceMethods<TInstance> {
  checkPassword(candidatePassword: string): Promise<boolean>;
  updatePassword(oldPassword: string, newPassword: string): Promise<TInstance>;
  resetPassword(password: string): Promise<TInstance>;
  createReset(): Promise<TInstance>;
  loginFail(): Promise<TInstance>;
  loginSuccess(): Promise<TInstance>;
  activateAccount(): Promise<TInstance>;
  deactivateAccount(): Promise<TInstance>;
}
export function checkPassword(candidatePassword: string) {
  return new Promise((resolve, reject) => {
    compare(candidatePassword, this.password, (err, isMatch) => {
      if (err) reject(err);
      else resolve(isMatch);
    })
  })
}
export function updatePassword(oldPassword: string, newPassword: string) {
  return new Promise((resolve, reject) => {
    this.checkPassword(oldPassword).then(isMatch => {
      if (!isMatch) {
        reject(new Error('Passwords Do Not Match'));
      }
      else {
        resolve(this.resetPassword(newPassword));
      }
    })
  })
}
export function resetPassword(password: string) {
  return new Promise ((resolve, reject) => {
    genSalt((err, salt) => {
      if (err) reject(err);
      else {
        hash(password, salt, (err, hash) => {
      if (err) reject(err);
      else {
        resolve(this.update({password: hash}));
          }
        })
      }
    })
  })
}
export function createReset() {
  return new Promise((resolve, reject) => {
    var now = new Date();
    now.setHours(now.getHours() + 1);
    crypto.randomBytes(48, (err, buf) => {
      if (err) return reject(err);
      var token = buf.toString('hex');
      resolve(this.update({ resetToken: token, resetExpire: now, status: 'reset' }));
    });
  })
}
export function loginFail () {
  return this.increment('failAttempts', { by: 1 })
  .then(user => {
    return user.reload()
  })
}
export function loginSuccess () {
  return this.update({failAttempts: 0})
}
export function activateAccount () {
  var activate = {
    failAttempts: 0,
    status: 'active',
    resetToken: null,
    resetExpire: null
  };
  return this.update(activate)
}
export function deactivateAccount () {
  var deactivate = {
    status: 'inactive'
  }
  return this.update(deactivate)
}

