import * as InstanceMethods from './';
import * as chai from 'chai';
import {genSaltSync, hashSync} from 'bcrypt';

var mockObject = {
  set: function(attr, value) {
    this[attr] = value;
  },
  update: function (args) {
    return new Promise((resolve, reject) => { resolve(this) }
  },
  save: function() {
    return new Promise((resolve, reject) => { resolve(this) })
  },
  password: null
}
describe ('prUser.InstanceMethods', () => {

  beforeEach(done => {
    mockObject.password = hashSync('password', genSaltSync())
    done()
  })
	it ('Should exist', () =>{
    chai.expect(InstanceMethods).to.exist
  })

  describe('checkPassword()', () => {
    it ('Pass with correct credentials', done => {
      InstanceMethods.checkPassword.apply(mockObject, ['password'])
      .then(isMatch => {
        chai.expect(isMatch).to.be.true
        done()
      })
      .catch(done)
    })
  it('Pass with Incorrect credentials', done => {
    InstanceMethods.checkPassword.apply(mockObject, ['Password'])
      .then(isMatch => {
        chai.expect(isMatch).to.be.false
        done()
      })
      .catch(done)
    })
  })

  describe('hashPassword()', () => {
    it('Reset a Password', done => {
    InstanceMethods.resetPassword.apply(mockObject, ['Password']).then(user => {
        chai.expect(user).to.exist
        done()
      })
      .catch(done)
    })
  })
  describe('updatePassword()', () => {
    it ('Update Password with correct Password', done => {
      Object.assign(InstanceMethods, mockObject)
      InstanceMethods.updatePassword.apply(InstanceMethods, ['password', 'newpassword'])
      .then(instance => {
        chai.expect(instance).to.exist
        done()
      })
      .catch(done)
    })
  })
})
