import * as sequelize from 'sequelize';
export interface IAttributes {
  email?: string;
  password?: string;
  resetToken?: string;
  resetExpire?: string;
  failAttempts?: string;
  status?: string;
}

export var attributes: sequelize.DefineAttributes = {
  email : {
    type: sequelize.STRING,
    unique: true,
    allowNull: false,
    validate: {
      isEmail: { msg: 'Not a Valid Email' }
    }
  },
  password : {
    type: sequelize.STRING,
    allowNull: false
  },
  resetToken : {
    type: sequelize.STRING,
  },
  resetExpire : {
    type: sequelize.DATE
  },
  failAttempts: {
    type: sequelize.INTEGER,
    defaultValue: 0
  },
  status: {
    type: sequelize.STRING,
    defaultValue: 'active'
  }
}






